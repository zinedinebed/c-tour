﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    public class GradeBook : GradeTracker
    {
        protected List<float> grades;

        //TextWritter is a good abstraction to  write information to textwtiter : console , a file ...etc
        override public void WriteGrades(TextWriter destination)
        {
            for (int i = 0; i < grades.Count; i++)
            {
                destination.WriteLine(grades[i]);
            }
        }
        override public void AddGrade(float grade)
        {
            grades.Add(grade);
        }
        public GradeBook()
        {
              grades = new List<float>();
              name = "Empty";
        }
        override public GradeStatistis ComputeStatistics()
        {
            GradeStatistis stat = new GradeStatistis();

            float sum = 0;

            foreach (var item in grades)
            {
                stat.highestGrade = Math.Max(item, stat.highestGrade);
                stat.lowestGrade = Math.Min(item, stat.lowestGrade);
                sum += item;
            }

            stat.AverageGrade1 = sum / grades.Count;

            return stat;
        }

        //because GradeBook inherit from IGradeTracker => must implement GetEnumerator
        // this really a powerful abstraction 
       // we are returning the pointer  of our list IEnumerator so we can after abstract an iterate throw book
       //without knowing what is the logic behind
       // RESUME : if we iterate throw a GradeBook => we are iterating throw his list :)
        public override IEnumerator GetEnumerator()
        {
            return grades.GetEnumerator();
        }


    }
}
