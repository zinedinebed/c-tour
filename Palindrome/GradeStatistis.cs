﻿namespace Palindrome
{
    public class GradeStatistis
    {

        public GradeStatistis()
        {

            highestGrade = 0;
            lowestGrade = float.MaxValue;
        }

        public float AverageGrade1 { get; set; }
        public float highestGrade { get; set; }
        public float lowestGrade { get; set; }

        public string LetterGrade
        {
            get
            {
                string result;
                if (AverageGrade1>=90)
                {
                    result = "A";   
                }
                else if (AverageGrade1 >= 80)
                {
                    result = "B";
                }
                else if(AverageGrade1 >= 70)
                {
                    result = "C";
                }
                else
                {
                    result = "D";
                }

                return result;
            }

        }
    }
}