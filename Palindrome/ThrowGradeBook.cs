﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    public class ThrowGradeBook : GradeBook
    {
        public override GradeStatistis ComputeStatistics()
        {

            float lowest = float.MaxValue;
            foreach (float item in grades)
            {

                lowest = Math.Min(item, lowest);

            }
            //remove the lowest
            grades.Remove(lowest);

            return base.ComputeStatistics();
        }

       


    }
}
