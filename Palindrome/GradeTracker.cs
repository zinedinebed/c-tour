﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    public abstract class GradeTracker : IGradeTracker
    {
        //abstract class can have method which are implemented

        public abstract void AddGrade(float grade); //let the implementation to the derived class
        public abstract GradeStatistis ComputeStatistics();
        public abstract void WriteGrades(TextWriter destination);

        //because IGradeTracker inherit from IEnumerble => GRadeTracker must implement : 
        //because we dont know how to implement GetEnumerator => will put it abstract
        public abstract IEnumerator GetEnumerator();
       

        public event NameChangedDelegate NameChangedD; // event is all the word key that we must use
        protected string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                //if (!String.IsNullOrEmpty(value))

                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }

                if (name != value)
                {
                    //delegatee
                    //NameChangedD(name, value); // solution with delegates

                    //now solution with event
                    NameChangedEventArgs args = new NameChangedEventArgs();
                    args.ExistingName = name;
                    args.NewName = value;
                    //I'm the  sender => sender = this
                    NameChangedD(this, args);

                }

                name = value;
            }
        }




    }
}
