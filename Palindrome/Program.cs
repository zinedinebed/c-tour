﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    class Program
    {
        public static void Main(string[] args)
        {
            //GradeBook book = CreateGradeBook();//can be replaced later by a factory or provider
            IGradeTracker book = CreateGradeBook();//can be replaced later by a factory or provider

            // GradeBook book = new GradeBook();

            book.AddGrade(91);
            book.AddGrade(90);
            book.AddGrade(80);

            IGradeTracker book2 = book; // hold reference to book 1
            book2.AddGrade(75); // same as book.AddGrade(75);

            book.WriteGrades(Console.Out);

            Console.WriteLine("List of grades throw IEnumerator");
            foreach (var grade in book)
            {

                Console.WriteLine(grade);
            }

            try
            {
                //everytime the name change in gradebook we call Onchanged name via NameChangedD delegate
                //if there is no subscriber this will generate a NullReferenceException
                book.NameChangedD += (OnCHangedName);

                Console.WriteLine("Please enter the name of the book : ");
                book.Name = Console.ReadLine();
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("Why did you enter an empty value ?? :(");
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Something wrong or maybe u forget to subscribe to an event");
            }

            GradeStatistis stats = book.ComputeStatistics();

            WriteResult("Average", stats.AverageGrade1);
            WriteResult("Highest", stats.highestGrade);
            WriteResult("Lowest", stats.lowestGrade);

            WriteResult("Grade : ", stats.LetterGrade);

            //when u know that an object containcs a Dispose method then u can use using() to handle this auto
            using (StreamWriter outPutFile = File.CreateText("Grade.txt"))
            {
                book.WriteGrades(outPutFile);
            }

        }

        private static IGradeTracker CreateGradeBook()
        {

            //like this if we call a method from Throw grade book which 
            ///have the same name in GradeBook , it s GradeBook method who ll be called
            return new ThrowGradeBook();
        }

        public static void OnCHangedName(object sender, NameChangedEventArgs args)
        {
            Console.WriteLine($"The name has changed from {args.ExistingName} to {args.NewName}");
        }

        public static void WriteResult(string description, float result)
        {
            //with this new feature we dont need to write variables
            Console.WriteLine($"{description}: {result:F2}");
        }

        public static void WriteResult(string Grade, string gradeLetter)
        {
            //with this new feature we dont need to write variables
            Console.WriteLine($"{Grade}: {gradeLetter}");
        }
    }
}
