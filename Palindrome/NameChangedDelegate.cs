﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    // now we will conver this : public delegate void NameChangedDelegate(string existingValue, string newName);
    // to the event standar convention (object sender, eventArgs)


    public delegate void NameChangedDelegate(object sender, NameChangedEventArgs args);


}
