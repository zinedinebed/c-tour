﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    interface IGradeTracker : IEnumerable
    {
        void AddGrade(float grade); //let the implementation to the derived class
        GradeStatistis ComputeStatistics();
        void WriteGrades(TextWriter destination);

        event NameChangedDelegate NameChangedD;
        string Name { get; set; }
    }
}
