﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    public class NameChangedEventArgs : EventArgs // muts derive from event args => change now our delegate
    {

        public string ExistingName { get; set; }
        public string NewName { get; set; }
    }
}
